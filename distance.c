//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float input()
{
	float a;
	scanf("%f",&a);
	return a;
}
float input1()
{
	float b;
	scanf("%f",&b);
	return b;
}
float distance(float x1,float y1,float x2,float y2)
{
	float distance;
	distance=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
	return distance;
}
void display(float x1,float y1,float x2,float y2,float ans)
{
	printf("The distance between two points with coordinates %.2f %.2f and %.2f %.2f is %.2f",x1,y1,x2,y2,ans);
}
int main()
{
    float ans,x1,y1,x2,y2;
    printf("enter the value of x1: ");
    x1=input();
    printf("enter the value of y1: ");
    y1=input1();
    printf("enter the value of x2: ");
    x2=input();
    printf("enter the value of y2: ");
    y2=input1();
    ans=distance(x1,y1,x2,y2);
    display(x1,y1,x2,y2,ans);
    return 0;
}
