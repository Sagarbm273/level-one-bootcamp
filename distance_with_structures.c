//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct point 
{
	float x;
	float y;
};
typedef struct point Point;
Point input()
{
	Point a;
	printf("enter the values of x and y ");
	scanf("%f %f ",&a.x,&a.y);
	return a;
}
float compute(Point p1,Point p2)
{    
	float distance;
    distance=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
    return distance;

}
void display(Point p1,Point p2,float distance)
{
    printf("The distance between the points p1 of coordinates %.2f and %.2f and p2 of %.2f and %.2f is %.2f",p1.x,p1.y,p2.x,p2.x,distance);
}
int main ()
{
    Point a,b;
    float dist;
    a=input();
    b=input();
    dist=compute(a,b);
    display(a,b,dist);
}
