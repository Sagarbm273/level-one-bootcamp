//WAP to find the sum of two fractions.
#include<stdio.h>
struct fraction
{
    int num;
    int deno;

};
typedef struct fraction fraction ;
fraction input()
{
    fraction n;
    printf("enter the values for num and deno : ");
    scanf("%d %d",&n.num,&n.deno);
    return n;
}
int calculate_gcd(int a,int b)
{
    if(b==0)
    {
        return a;
        
    }else
    {
        return calculate_gcd(b,a%b);
    }
  
}
fraction compute(fraction a ,fraction b)
{
    fraction sum;
    sum.num=(a.num*b.deno+a.deno*b.num);
    sum.deno=(a.deno*b.deno);
    int gcd=calculate_gcd(sum.num,sum.deno);
    sum.num=sum.num/gcd;
    sum.deno=sum.deno/gcd;
    return sum;
}
void display(fraction a,fraction b,fraction sum)
{
    printf("The sum of %d/%d and %d/%d is %d/%d",a.num,a.deno, b.num, b.deno, sum.num,sum.deno);
}

int main()
{
    fraction a,b,sum;
    a=input();
    b=input();
    sum=compute(a,b);
    display(a,b,sum);
    return 0;
}