//Write a program to find the volume of a tromboloid using one function

#include<stdio.h>
int main()
{
	float h,b,d,volume;
	printf("Enter the value for h,b and d:");
	scanf("%f%f%f",&h,&b,&d);
	volume=((1.0/3.0)*((h*b*d))+(d/b));
	printf("The volume of Tromboloid with parameters %.2f, %.2f and %.2f is %.2f",h,b,d,volume);
	return 0;
}
